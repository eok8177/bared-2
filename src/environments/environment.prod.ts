export const environment = {
  production: true,
  apiUrl: 'https://api.bared.com.au/',
  awsS3Url: 'https://s3-ap-southeast-2.amazonaws.com/bared/'
};
