export class HomeItems {
    id: number;
    image: string;
    label: string;
    sort: number;
    url: string;
}