import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent, FooterComponent } from './shared';

import { ImageS3 } from './awsS3Pipes.pipe';
// import { HeaderComponent } from './header/header.component';

import { MatInputModule } from '@angular/material';

@NgModule({
  exports: [
    MatInputModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    ImageS3,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
