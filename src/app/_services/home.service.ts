import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;

  getItems() {
    return this.http.get(this.apiUrl + 'homepage');
  }
}
