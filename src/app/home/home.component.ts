import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { HomeItems } from '../_models';
import { HomeService } from '../_services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  items: HomeItems[] = [];

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.homeService.getItems()
      .pipe(first())
      .subscribe((items:any) => {
        this.items = items.result;
      });
  }

}
