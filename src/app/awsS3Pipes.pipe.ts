import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../environments/environment';

@Pipe({name: 'imageS3'})
export class ImageS3 implements PipeTransform {
  transform(fileName: string, placeholder: string = 'home'): string {
      switch (placeholder) {
          case 'banner':
              placeholder = '/assets/images/bared/banner-placeholder.jpg';
              break;
          case 'tile':
              placeholder = '/assets/images/bared/tile-placeholder.jpg';
              break;
          case 'product':
              placeholder = '/assets/images/bared/product-placeholder.jpg';
              break;
          default:
              placeholder = '/assets/images/bared/homepage-placeholder.jpg';
              break;
      }
    return fileName ? (environment.awsS3Url + fileName) : placeholder;
  }
}